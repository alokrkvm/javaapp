# JavaApp

A sample Java web application

# Build Docker Image

Run "docker build -t javaapp ."

# Run Docker Container

Run "docker run -d -p 9090:9090 javaapp:latest"

# Check Application Status

curl localhost:9090 or hit browser with port 9090

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Sample Webapp CRUD Example </title>
  </head>
  <body>
    <h2>Sample WebApp CRUD Example for CI</h2>
    <h2>1. <a href="employees.html">List of Employees</a></h2>
    <h2>2. <a href="add.html">Add Employee</a></h2>
    <h2>3. <a href="fileUpload.html">Upload File</a></h2>
    <h2>4. <a href="listImages.html">List Images</a></h2>
  </body>
</html>
