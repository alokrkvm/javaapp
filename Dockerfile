FROM tomcat:8.5.16-jre8-alpine
RUN apk update
RUN apk add git 
RUN apk add openjdk8
RUN apk add maven
RUN git clone https://gitlab.com/ot-interview/javaapp.git
RUN cd javaapp && mvn clean &&  mvn package
RUN ["rm", "-fr", "/usr/local/tomcat/webapps/ROOT"]
RUN cp javaapp/target/Spring3HibernateApp.war .
RUN mv Spring3HibernateApp.war webapps/ROOT.war
RUN sed -i 's/port="8080"/port="9090"/' /usr/local/tomcat/conf/server.xml
CMD ["catalina.sh","run"]
